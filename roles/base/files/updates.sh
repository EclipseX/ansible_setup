#! /bin/bash

for action in update upgrade dist-upgrade autoclean autoremove; do
  sudo apt-get -y $action
done
