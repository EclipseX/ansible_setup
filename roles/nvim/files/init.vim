"Plug-ins
call plug#begin('~/.config/nvim/plugged')
" Tools
    Plug 'vifm/vifm.vim'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'ctrlpvim/ctrlp.vim'
    Plug 'itchyny/vim-gitbranch'
    Plug 'tpope/vim-fugitive'
    Plug 'itchyny/lightline.vim'
    Plug 'vimwiki/vimwiki'
" Syntax
    Plug 'tpope/vim-markdown'
    Plug 'ap/vim-css-color' "Displays a preview of colors with CSS
    Plug 'vim-scripts/fountain.vim'
    Plug 'jiangmiao/auto-pairs'
    Plug 'alvan/vim-closetag'
" Color-schemes
    Plug 'arcticicestudio/nord-vim'
    Plug 'joshdick/onedark.vim'
    Plug 'gruvbox-community/gruvbox'
call plug#end()

"General Settings
set encoding=UTF-8
filetype plugin indent on                                 " Enabling Plugin & Indent
syntax on                                                 " Turning Syntax on
set number
set relativenumber
set nu rnu
set autoread wildmode=longest,list,full
set spell spelllang=en_us
set backspace=indent,eol,start confirm
set shiftwidth=4 autoindent smartindent tabstop=4 softtabstop=4 expandtab
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
set hls is ic
set laststatus=2 cmdheight=1
au BufRead,BufNewFile *.fountain set filetype=fountain
set splitbelow splitright
set nobackup nowritebackup nocursorline
set cursorline
set mouse-=a
set tabstop=2 softtabstop=2
set background=dark
set noswapfile
set nobackup                                             " Recommended by coc
set nowritebackup                                        " Recommended by coc
set clipboard=unnamedplus                                " Copy paste between vim and everything els

"Key-bindings
let mapleader=" "
nnoremap <leader>n :Explore<CR>
nnoremap <leader><Space> :CtrlP<CR>
nnoremap <leader>, :vsplit %<CR>
nnoremap <leader>. :split %<CR>
nnoremap <C-g> :set spelllang=en_us<CR>
nnoremap <C-s> :source ~/.config/nvim/init.vim<CR>

xnoremap J :move '>+1<CR>gv-gv
xnoremap K :move '<-2<CR>gv-gv

nnoremap Q <nop>

nnoremap <leader>h <C-W>h
nnoremap <leader>j <C-W>j
nnoremap <leader>k <C-W>k
nnoremap <leader>l <C-W>l

"You can't stop me
cmap w!! w !sudo tee %

"Color Settings - F-Keys can change the colorscheme
colorscheme nord
map <F1> :colorscheme nord<CR>
map <F2> :colorscheme onedark<CR>
map <F3> :colorscheme gruvbox<CR>
